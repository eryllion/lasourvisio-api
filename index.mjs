import express from 'express';
import axios from 'axios';
import yargs from 'yargs';
import fs from 'fs';
import https from 'https';
import { Server  } from 'socket.io';
import moment  from 'moment'; 
import 'moment-timezone'; 
import { TZ } from './config/index.mjs';

moment.tz.setDefault( TZ ); 
axios.defaults.headers.common = {
    "Content-Type": "application/json"
}

var yarg = yargs( process.argv.slice(2) ).argv;
const { p = 801 } = yarg;

let serverPort = p;

import { cert_pem , cert_key , httpsOptions, option_sockets } from './config/index.mjs'; 
import { Store, UserSockets , Log } from './lib/index.mjs';

const LoadApp = async () =>  {
    
    Store.app = await express();
    Store._server = await https.createServer( httpsOptions, Store.app );
    Store.io = await new Server( Store._server, option_sockets );
    await import('./server.mjs'); 
    Store.Rooms.CreateRoom( 2000 );
    Store.Rooms.CreateRoom( 2001 );
    Store.Rooms.CreateRoom( 2002 );
    Store.Rooms.CreateRoom( 2003 ); 
    
    Store._server.listen( serverPort, function() {
        console.log('Server start: ' + moment().format('YYYY-MM-DD HH:mm:ss'))
        console.log( 'Server Started', 'LASOUR VISIO API is up and running at '+serverPort+' port ' );
    } );
    
    Store.io.on( 'connection', function( socket ) {
        console.log('Connection from socket: ' + socket.id );
        Log.add('NEW_SOCKET', { socketid: socket.id }); 
        Store.handles[socket.id] = new UserSockets(socket);
        console.log( "ROOMS", Store.Rooms );
    } );
    //await import('./locals/index.mjs');
   // if( serverPort === 2002 ) import('./test.mjs');
}


LoadApp();







