import {  InArray, IsArray,  IsObject } from '../lib/index.mjs';
import { ESC } from './Escape.mjs';

export const Where = {
    _where: [],
    _operand: ['_and', '_or', '_gt', '_gte', '_lt', '_lte', '_ne', '_between', '_notBetween', '_in', '_notIn', '_like', '_notLike'],
	
	_and:   function ( _k, values ) {
		let tmp = [];
		const d =  this.add( values , 'AND');
		return d;
	},
	_or: function ( _k, values ) {
    	let tmp = [];
    	const d =  this.add( values , 'OR');
    	return d;
	},
	_gt:   function (key, values ) {
    	return " " + key + " > " + ESC(values  )+ " "
	},
	_gte:  function (key, values ) {
    	return " " + key + " >= " + ESC(values  )+ " "
	},
	_lt:   function (key, values ) {
    	return " " + key + " < " + ESC(values  )+ " "
	},
	_lte:   function (key, values ) {
    	return " " + key + " <= " + ESC(values  )+ " "
	},
	_ne:   function (key, values ) {
    	return " " + key + " != " + ESC(values  )+ " "
	},
	_between:   function (key, values ) {
        let tmp = [];
        tmp.push( ESC(values[0])  );
        tmp.push( ESC(values[1])  );
    	return " ( " + key + " BETWEEN " + tmp.join( ' AND ' ) + " ) "
	},
	_notBetween:   function (key, values ) {
        let tmp = [];
        tmp.push( ESC(values[0])  );
        tmp.push( ESC(values[1])  );
    	return " ( " + key + " NOT BETWEEN " + tmp.join( ' AND ' ) + " ) "
	},
	_in:  function (key, values ) {
        let tmp = [];
        for( let i in values ) {
            tmp.push(  ESC(values[i]  ) );
        }
    	return " ( " + key + " IN ( " + tmp.join( ' , ' ) + " ) ) "
	},
	_notIn:   function (key, values ) {
        let tmp = [];
        for( let i in values ) {
            tmp.push( "" + ESC(values[i]  )+ "" );
        }
    	return " ( " + key + " NOT IN ( " + tmp.join( ' , ' ) + " ) ) "
	},

	_like:   function (key, values ) {
    	return " " + key + " LIKE " + ESC(values  )+ " "
	},

	_notLike:   function (key, values ) {
    	return " " + key + " NOT LIKE " + ESC(values  )+ " "
	},

    _eq:  function (key, value) {
			return " " + key + " = " + ESC(value  )+ " " ;
    },

	add:  function ( conds, o =  'AND' ) {
		
        if( typeof conds === 'object' ) {
        	let tmp = [];
            for( let key in conds ) {
                let value = conds[ key ];
                if( InArray( key, this._operand ) ) {
					tmp.push(  this[ key ]( key, value ) ) ;
                } else {
                   
					if( !IsObject(value) && !IsArray(value) ) {
						tmp.push(   this._eq(key, value ));
					} else {
						
						for( let vkey in value) {
							if( InArray( vkey , this._operand )) {
								tmp.push(   this[ vkey] ( key, value[vkey] ));
							}
						}
					}
                
                }
            }
            if (o !== false) {
            	tmp = "( " + tmp.join( ' ' + o + ' ' ) + " )";
            	return tmp;
			}
        }


	}
}
