import { empty, GetNow } from '../lib/index.mjs';
import mysql from 'mysql';
import util from 'util';
import { Where } from './Where.mjs';
import { DATABASE } from '../config/index.mjs';
import { Escape } from './Escape.mjs'; 


const database = DATABASE;

export const DB = {
	connected: false,
	co: false,
	sql: '',
	joins: [],
	wheres: [],
	orders: [],
	values: [],
	limit: '',
	group_by : '',
	insertDuplicates: [],
	s: 'select',
	pool: true,
	/**
	 * Connect to Database
	 * @param { boolean } force 
	 * @returns {this}
	 */
	Connect : async function(force = false)  {
		if (this.connected) return true;
		const _database = { ...database, connectionLimit : 1000, };
		this.pool = await mysql.createPool(_database);
		this.connected = true;
        this.__where = Where;
		return this;  
	},
	/**
	 * Delete function
	 * @param { string } table 
	 * @returns {this}
	 */
	Delete: function(table) {
		this.s = 'delete';
		this.sql = 'DELETE FROM ' + Escape( table ) + ' ';
		return this;
	},
	/**
	 * Escape queries
	 * @param { string } str 
	 * @returns { string }
	 */
	Escape: function(str) {
		return Escape( str ); 
	},
	/**
	 * Leave SQL Connection
	 * @returns { boolean }
	 */
	Exit: function() {
		try {
			if (typeof this.co !== 'undefined') this.co.release();
		} catch( e ) {

		} finally {
			return true;
		}
	},
	/**
	 * Group BY
	 * @param {string} by 
	 * @returns {this}
	 */
	Group( by ) {
		return this.GroupBy( by ); 
	},
	GroupBy: function (by) {
		this.group_by = by;
		return this;
	},
	/**
	 * Insert Query
	 * @param {string} table 
	 * @returns {this}
	 */
	Insert: function(table) {
		this.s = 'insert';
		this.sql = "INSERT INTO "+ table + " ";
		return this;
	},
	/**
	 * JOIN LEFT REQUEST
	 * @param {string|object} request 
	 * @returns {this}
	 */
	Left: function(request) {
		if (typeof request ==='object') {
			for(let i in request) {
				this.joins.push(' LEFT JOIN ' + request[i]);
			}
		} else{
			this.joins.push(' LEFT JOIN ' + request);
		}
		return this;
	},
	/**
	 * Limit REQUEST
	 * @param {string} limit 
	 * @returns {this}
	 */
	Limit: function(limit) {
		this.limit = limit;
		return this;
	},
	/**
	 * Add On Duplicate Keys 
	 * @param {object|array|string} values 
	 * @returns {this}
	 */
	OnDuplicate: function(values) {
		if( typeof values === 'object') {
			for( let i in values ){
				this.insertDuplicates.push(
					" `" + i + "` = VALUES( `" + i + "` ) "
				);
			}
		} else if ( typeof values === 'string') {
			this.insertDuplicates.push( values );
		} else {
			for( let i in values ){
				var key = values[i];
				this.insertDuplicates.push(
					" `" + key + "` = VALUES( `" + key + "` ) "
				);
			}
		}
		return this;
	},
	/**
	 * Order By
	 * @param {string|object} order 
	 * @returns {this}
	 */
	Order: function(order) {
		if (typeof order === 'object') {
			this.orders = order;
		} else {
			this.orders.push(order);
		}
		return this;
	},
	/**
	 * Execute Query
	 * @param {string} sql 
	 * @returns { promise } result
	 */
	Query: async function(sql) {
		await this.Connect();
		const _this = this;
		return new Promise( async function(resolve, reject){
			var dbConn  = _this.pool.getConnection( function (err, connection) {
				if (err) {
					reject(err);
					throw err;
					if ( typeof  connection !== 'undefined') connexion.release();
				} else {
					_this.co = connection;
					connection.query(sql, async function(err2, result) {
						if (err2) {
							reject(err2);
						} else {
							resolve(JSON.parse(JSON.stringify(result)));
						}
						connection.release();

					})
				}
			})

		});

	},
	
	/**
	 * Set Custom SQL QUERY
	 * @param { string } sql 
	 * @returns { this }
	 */
	Request : function ( sql ) {
		this.sql = sql;
		return this; 
	},
	/**
	 * Reset vars
	 */
	Reset: function() {
		this.sql = ""; 
		this.wheres = [];
		this.joins = [];
		this.orders = [];
		this.limit = '';
		this.group_by = '';
		this.insertDuplicates = [];
	},
	/**
	 * return result 1 line
	 * @param { boolean } debug
	 * @returns { object } result 
	 */
	Result: async function(debug = false) {
		const row = await this.Results( debug );
		if (row.length == 0) return false;
		return row[0];
	},
	/**
	 * Results (mutil-line)
	 * @param { boolean } debug 
	 * @returns { array } results
	 */
	Results: async function(debug = false ) {
		this.Connect();

		let sql=  this.sql;

		sql+= ' ' + this.joins.join(' ') + ' ';
		if (this.wheres.length > 0) {
			sql += ' WHERE ' + this.wheres.join(' AND ');
		}
		if (this.group_by != '' ) {
			sql+= ' GROUP BY ' + this.group_by;
		}
		if (this.orders.length > 0) {
			sql+= ' ORDER BY ' + this.orders.join(', ');
		}
		sql+= ' ' + this.limit;
		if (debug) console.log(sql);

		this.Reset();
		return await this.Query(sql);
	},
	/**
	 * Exec REQUEST for Update, Delete, Insert 
	 * @param { boolean } debug 
	 * @param { boolean } exec  if ( false ) do not execute the request
	 * @returns 
	 */
	Run : async function(debug = false, exec = true ) {
		let sql = this.sql;
		if (this.wheres.length > 0) {
			sql += ' WHERE ' + this.wheres.join(' AND ');
		}
		if (this.insertDuplicates.length > 0) {
			sql+= ' ON DUPLICATE KEY UPDATE ' + this.insertDuplicates.join(',');
		}
		if (debug) console.log(sql);
		if( !exec ) return true; 
		this.Reset(); 
		const d =  await this.Query(sql);

		return d;
	},

	/**
	 * Select REQUEST
	 * @param { string } table 
	 * @param { string } alias 
	 * @returns { this }
	 */
	Select: function(table, alias = '') {
		this.s = 'select';
		this.Reset();
		this.sql = 'SELECT * FROM `' + Escape( table ) + '`  ' + ( alias == '' ? '' : ' AS ' + alias );
		return this;
	},
	/**
	 * SQL QUERY To String
	 * @returns { string }
	 */
	ToString : async () => {
		await this.Run( false, false );
		return this.sql ; 
	},
	/**
	 * Update REQUEST
	 * @param {string} table 
	 * @returns { this }
	 */
	Update: function(table) {
		this.s = 'update';
		this.sql = "UPDATE " + Escape( table ) + " SET ";
		return this;
	},
	/**
	 * Set Values for Insert / Update
	 * @param { object } values 
	 * @returns { this }
	 */
	Values: function(values) {
		let sql = this.sql;
		if (this.s === 'insert') {
			var keys = [];
			var vals = [];
			for(let i in values ) {
				const value = Escape(values[i]);
				keys.push( '`'+ i +'`' );
				vals.push( value );
			}
			sql += '( ' + keys.join(',') + ' ) ';
			sql += ' VALUES ( ' + vals.join(', ') + ' )';

		} else {
			var params = [];
			for(let i in values) {
				if( values[i] === null ) values[i] = '';
				const value = Escape(values[i]);
				params.push( '`'+ i +'` = ' + value + ' ' );
			}
			sql += ' ' + params.join(', ') + ' ';
		}
		this.sql = sql;
		return this;
	},
	/**
	 * Which select fields
	 * @param { string } str
	 * @returns { this }
	 */
	Which: function(str) {
		this.sql = this.sql.replace('*', ' ' + str + ' ');
		return this;
	},
	/**
	 * REQUEST WHERE CONDITIONS
	 * @param { string|object } conds 
	 * @returns { this }
	 */
	Where: function(conds) {
		console.log(' A ', typeof this.Result  );
		if (typeof conds === 'string') {
			this.wheres.push(conds);
			return this;
		}
		const _this = this;
		const sql_cond = Where.add(conds);
		this.wheres.push( sql_cond );
		return this;
	}
	
};
//connect DataBase
DB.Connect();
