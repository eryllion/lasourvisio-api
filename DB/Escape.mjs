import { DB } from './DB.mjs';
import mysql from 'mysql';
import { empty, InArray, IsArray, IsNumber, IsObject, IsString} from '../lib/index.mjs';

/**
 * Escape String
 * @param { string  }  str 
 * @returns { string }
 */
export const ESC = ( str ) => {
	if( IsNumber( str ) ) return str;
	if (IsArray( str ) || IsObject( str ) ) return '';
	return mysql.escape( str );
}
export const Escape = (str) => { return ESC(str); } 
