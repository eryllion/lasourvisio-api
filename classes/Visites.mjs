import {DBModel, empty, number_format, GetNow,  Store } from '../lib/index.mjs';
import { DB } from '../DB/index.mjs';
import { VISITES_DB } from '../config/database.mjs'; 

export class Visites extends DBModel {
	/**
	 * 
	 * @param {number} id 
	 * @returns { promise }
	 */
	constructor(id = 0) {
		super(id);

		this._tb = VISITES_DB ;

		return new Promise(async (resolve, reject) => {
			await this.__Init(id);
			resolve(this);
		});

	}
	/**
	 * Add a new Visite
	 * @param { object } param 
	 * @returns { boolean }
	 */
	static async NewVisit ({ room, uuid, username, socketid  }) {
		var datecreate = await GetNow();
		DB.Insert( VISITES_DB ).Values({
			datecreate,
			uuid,
			room,
			username,
			socketid, 
		}).Run();
		return true;
	};
	/**
	 * User leave Server
	 * @param { object } params
	 * @returns { boolean }
	 */
	static async Leave( { socketid  } ) {
		var dateleaved = await GetNow();
		DB.Update( VISITES_DB ).Values({
			dateleaved
		}).Where({
			socketid
		}).Run();
		return true;
	}
	/**
	 * Get UUID By SocketId
	 * @param { string } socketid 
	 * @returns { string } uuid
	 */
	static async GetUUID( socketid ) {
		const  { uuuid =  '' }= await DB.Select(VISITES_DB).Where({ socketid }).Result();
		return uuid; 
	}
    


}