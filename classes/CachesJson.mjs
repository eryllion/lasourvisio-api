import { GetNow, empty } from "../lib/index.mjs";
import fs from 'fs'; 
import { CACHEJSON } from "../config/Paths.mjs";

export class CachesJson  {

    /**
     * Get Cache Date from filename 
     * @param { string } file 
     * @returns json
     */
    static async GetCache( file) {
        file = file + '.json'; 
        if( file.indexOf('*' ) !== -1) {
            return false; 
        }
        var c = await fs.existsSync( file ); 
        if( !c  ) return false;
        var json = await fs.readFileSync(  file );
        return json; 
    }
    /**
     * Set Cache Data
     * @param { string } file 
     * @param { string  } datas ( json stringified ) 
     * @returns 
     */
    static async SetCache( file, datas ) {
        await fs.writeFileSync( file + '.json' , datas ); 
        return true;
    }
    /**
     * Delete Cache File
     * @param {string} key 
     * @param {string} source 
     * @returns 
     */
    static async DelCache( key, source = 'default') {
        var dir = CACHEJSON + source + "/";
        if( await fs.existsSync( dir ) === false ) return false; 
        const file = dir + key + ".json";
        if( await fs.existsSync( file ) === false ) return false;
        await fs.unlinkSync( file );
        return true; 
    }
} 