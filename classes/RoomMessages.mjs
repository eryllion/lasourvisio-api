import {DBModel, empty, number_format, GetNow,  Store } from '../lib/index.mjs';
import { DB } from '../DB/index.mjs';
import { ROOM_MESSAGES_DB } from '../config/database.mjs'; 

export class RoomMessages extends DBModel {
	/**
	 * 
	 * @param {number} id 
	 * @returns { promise }
	 */
	constructor(id = 0) {
		super(id);
		this._tb = ROOM_MESSAGES_DB ;
		return new Promise(async (resolve, reject) => {
			await this.__Init(id);
			resolve(this);
		});
	}
	/**
	 * Add a new Message in Room 
	 * @param { object } param
	 * @returns {boolean}
	 */
	static async addNew({ room, username, message, socketid , uuid  }) {
		var datecreate = await GetNow();
		if( empty( username ) || empty( message ) ) return false;
		DB.Insert( ROOM_MESSAGES_DB ).Values({
			datecreate,
			room,
			username, message,
			socketid ,
			uuid
		}).Run();
		return true;
	}

    


}