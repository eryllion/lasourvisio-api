import {DBModel, empty,  GetNow,  Store } from '../lib/index.mjs';
import { DB } from '../DB/index.mjs';
import { SOCKETS } from '../config/database.mjs'; 
import { IsNumber } from '../lib/Tools.mjs';

export class Sockets extends DBModel {
	/**
	 * 
	 * @param {number} id 
	 * @returns {promise}
	 */
	constructor(id = 0) {
		super(id);
		this._tb = SOCKETS;
		return new Promise(async (resolve, reject) => {
			await this.__Init(id);
			resolve(this);
		});
	}
	/**
	 * Get Socket Class by UID
	 * @param { integer } uid 
	 * @returns Socket Class
	 */
	static async GetByUid( uid ) {
		if (empty( uid ) ) return false;
		uid = Number( uid ); 
		const test = await DB.Select(SOCKETS).Where({ uid }).Result();
		const { socketid = false } = test;
		if ( !socketid ) return false;
		return await new Sockets( test.id );
	}
	/**
	 * Get Socket Class by UUID
	 * @param { string } uuid 
	 * @returns Socket Class
	 */
	static async GetByUuid( uuid ) {
		if (empty( uuid )) return false;
		const test = await DB.Select(SOCKETS).Where({ uuid }).Result();
		const { socketid = false } = test;
		if ( !socketid ) return false;
		return await new Sockets( test.id );
	}
	/**
	 * Add a new Socket
	 * @param { object } param 
	 * @returns Socket Class
	 */
	static async NewSocket({socketid, etat = 0, uuid = '', uid = 0 }) {
		var dateNow = await GetNow();
		let S = await new Sockets();
		S._datas = { socketid, datecreate: dateNow, lastconnect: dateNow , etat, uuid, uid: 0  };
		await S.Save();
		return S;
	}

	/**
	 * Get Socket By socket id
	 * @param { string } socketid 
	 * @returns Socket Class
	 */
	static async InitBySocketId( socketid ) {
		if( empty( socketid )) return false;
		const test = await DB.Select(SOCKETS).Where({ socketid }).Result();
		if ( empty(test.id)) {
			return await Sockets.NewSocket({ socketid, etat: 0, uuid: '', uid: 0  });
		} else {
			return await new Sockets( test.id );
		}
	}
	/**
	 * Login/update Socket
	 * @param { param } param
	 * @returns { boolean }
	 */
	static async LoginSocket({ socketid, uid, uuid, etat  }) {
		if ( empty(socketid)) return false;
		let current = false;
		if( !empty( uuid)) {
			current = await Sockets.GetByUuid(uuid);
		}
		if(!current ) {
			if (!empty(uid)) {
				current = await Sockets.GetByUid(uid);
			}
		}
		if( !current ) {
			current = await Sockets.InitBySocketId(socketid);
		}
		current._datas = {
			...current._datas, 
			uid, uuid, etat, socketid ,
			lastconnect: await GetNow()
		};
		await current.Save();
		return true;
	}
	/**
	 * Send event to socket
	 * @param uid 
	 * @param event 
	 * @param datas 
	 * @returns 
	 */
	static async Call( uid, event, datas ) {
		if( empty(event) || empty(uid) ) return false;
		const test = await DB.Select( SOCKETS ).Where({ uid }).Result();
		const { socketid = false } = test;
		if( !socketid ) return false;
		Store.io.to(socketid).emit( 'APP-RECEIVE', { event, datas } );
	}
}
