import { empty } from './index.mjs';
import { SCHEMAS  } from '../config/index.mjs'; 
import fs from 'fs';

/**
 * Get Schema
 * @param {string} key 
 * @returns {object}
 */
export const GetSchema = async  ( key ) => {
    if( empty(key)) return { }
    var file = SCHEMAS + key + '.json';
    const c = await fs.existsSync( file) ;
    if( !c ) return {};
    var json = await JSON.parse( fs.readFileSync( file ) );
    return json; 
}