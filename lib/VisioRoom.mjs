import { Store, RoomJson } from './index.mjs'; 
import SemanticSDP from 'semantic-sdp';
const { SDPInfo, MediaInfo } = SemanticSDP; 
import { Visites , RoomMessages } from '../classes/index.mjs'; 


export class VisioRoom {
    /**
     * constructor
     * @param {number} roomId 
     */
    constructor( roomId = 1 ) {
        this.roomId = roomId ;
        this.users = [];
        this.open = false;
        this.messages = [ ]; 
        RoomJson( this.roomId , { } ); 
    }

    /**
     * Close Room
     * 
     */
    async CloseRoom() {
        console.log('Close ROOM ' + this.roomId ); 
        this.open = false;
        Store.io.to( this.roomId ).emit('room-closed', { }); 
        var c = await RoomJson( this.roomId , { etat : 0 });
        this.messages = []; 
    }
    /**
     * Has Publisher
     * @param {array} excludes 
     * @returns {boolean}
     */
    async HasPublisher( excludes = [] ) {
        for ( let u of this.users ) {
            const { type, socketId } = u;
            if( excludes.indexOf( socketId ) === -1 ) {
                if ( u.type === 'publisher' || u.type === 'guide') return true;
            }
        }
        return false; 
    }
    /**
     * Join Room
     * @param { object } params { socket, username, type, uuid }
     * @param {callback} resolve 
     * @returns {boolean}
     */
    async Join( {  socket, username, type, uuid  }, resolve = () => {}) {
        var user = { socketId : socket.id , username , type , uuid } ;  
        var nb = await this.NbViewers();
        if ( nb > 3 ) {
            resolve( { etat : 201 , nb  } ); 
            return false;
        }

        this.users.push( user );
        var nb = await this.NbViewers(); 
        Store.io.to( this.roomId ).emit("user-joined" , { user, nb } ); 
        socket.join( this.roomId ); 
        RoomJson(this.roomId , { viewers: nb }); 
        
        if( user.type === 'publisher' || user.type === 'guide') this.OpenRoom(); 
        socket.on('disconnect' , () => {
            this.Leave( { ...user, socket } ); 
        })
        
        Visites.NewVisit({ uuid, room: this.roomId, username , socketid : socket.id}); 
        resolve( { etat : 200 , open: this.open, remoteUsers : this.users, nb  } ); 
        return true; 
    }

    /**
     * Leave Room
     * @param { object } params { socket, username, type}
     * @param {callback} resolve 
     */
    async Leave( { socket, username, type  }, resolve = () => {} ) {
        var tmp = []; 
        const uuid = await Visites.GetUUID( socket.id ); 
        var user = { socketId : socket.id , username , type , uuid } ;  
        for( let i in this.users ) {
            var u = this.users[ i ];
            if ( u.socketId !== socket.id ) {
                tmp.push( u );
            }
        }
        this.users = tmp;
        var nb = await this.NbViewers() ;

        Visites.Leave({ socketid: socket.id })
        Store.io.to( this.roomId ).emit('user-leaved', { user, nb, uuid  } );

        RoomJson(this.roomId , { viewers: nb }); 
        const c = await this.HasPublisher(); 
    }

    /**
     * Nb Viewers 
     * @returns {number}
     */
    async NbViewers() {
        var nb = 0;
        for ( let i in this.users )  {
            if( this.users[ i ].type !== 'guide') nb++;
        }
        return nb; 
    }

   
    /**
     * Open Room
     */
    async OpenRoom() {
        this.open = true;
        Store.io.to( this.roomId ).emit('room-opened', { }); 
       await RoomJson( this.roomId , { etat : 1 });
    }
    /**
     * Web Rtc 
     * @param { object } args 
     * @param { socket } socket 
     * @param {callback} callback 
     */
    async WebRtc( args, socket, callback = () => { } ) {
        const { task } = args;
        switch( task ) {
            case 'sdp' :
                var sdp = args.sdp ;
                var tmp = SDPInfo.process (sdp );
                var video = tmp.getMedia("video");
                video.setBitrate( 200000);
                sdp = tmp.toString(); 
                callback( { sdp } )
            break;
            case 'answer' :
                Store.io.to( args.to ).emit( 'room-webrtc-r', args );
            break;
            case 'offer' :
              
                Store.io.to( args.to).emit( 'room-webrtc-r', args );
            break;
            case 'candidate' :
                
                Store.io.to( args.to ).emit( 'room-webrtc-r', args );
            break;
            case 'new-message': 
                const { message } = args;
                this.messages.push( message ); 
                
                RoomMessages.addNew({ 
                    room: this.roomId,
                    username : message.pseudo,
                    message: message.message ,
                    socketid : socket.id ,
                    uuid: message.uuid
                })

            
                Store.io.to( this.roomId ).emit('ROOM-ON-MESSAGE', { message  }); 
            break;
            case 'livestop':
                console.log( 'livestop from ' + socket.id ); 
                
                const c = await this.HasPublisher( socket.id ); 
                if ( !c )  this.CloseRoom(); 
                
                
            break;
            case 'livestart' : 
                console.log('LIVE STARTED IN ' + this.roomId + ' BY ' + socket.id ); 
                Store.io.to( this.roomId ).emit('room-webrtc-r', args  ); 
            break;
            case 'orientation-changed': 
                console.log('ORIENTATION', args.deg );  
                Store.io.to( this.roomId ).emit('room-webrtc-r', args  ); 
            break;
           

        }
    }
}