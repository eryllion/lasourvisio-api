import { Store, VisioRoom } from './index.mjs';


class RoomClass {
    constructor( ) {
        this.list = [];
    }
    /**
     * Create Room
     * @param  roomId
     */
    CreateRoom =  async ( roomId ) => {
        const Room = await new VisioRoom( roomId );
        this.list.push( Room );
    }

    /**
     * Get Room By Id
     * @param roomId 
     * @returns {object | boolean false }
     */
    GetRoom( roomId ) {
        for( let Room of this.list ) {
            if( Room.roomId === roomId ) return Room;
        }
        return false;
    }
    /**
     * Add Events to Sokcet
     * @param {object} socket 
     */
    async Events (socket) {
        socket.on( 'join-room', ( args, callback = () => {} ) => {
            var Room = this.GetRoom( args.roomId );
            if( Room !== false ) Room.Join( { ...args , socket }, callback  );
        } );
        socket.on( 'room-webrtc', ( args, callback = () => {} ) => {
            console.log( args  )
            var Room = this.GetRoom( args.roomId );
            if( Room !== false ) Room.WebRtc( args , socket , callback  );
        } );
    }
}
Store.Rooms = new RoomClass(); 
