import { DB }  from '../DB/index.mjs';
import { CONFIG_TB } from '../config/database.mjs'; 
 
/**
 * Get config option
 * @param {string} key 
 * @param {string} defaut = ''  
 * @returns { string } value 
 */
export async function get_option(key, defaut = '') {
	const test = await DB.Select( CONFIG_TB ).Where({
		cfg_key: key
	}).Result();
	if (typeof  test.cfg_key === 'undefined') return defaut;
	return test.cfg_value;
}

/**
 * Set Config Option
 * @param {string} key 
 * @param {string} value 
 */
export async function set_option(key, value) {
	const test = await DB.Select( CONFIG_TB ).Which('COUNT(cfg_key) as X ').Where({ cfg_key : key }).Result();
	if (test.X == 0) {
		await DB.Insert( CONFIG_TB ).Values({ cfg_key: key, cfg_value: value }).Run();
	} else {
		await DB.Update( CONFIG_TB ).Values({ cfg_value : value }).Where({ cfg_key: key }).Run();
	}
}
