import { Store } from './Store.mjs';
import { empty } from './Tools.mjs';

class ApiClass {

	constructor() {
		this._paths = { };
	}
	/**
	 * get
	 * @param { string } path 
	 * @param {callback} func 
	 */
	async get(path, func ){
		this._paths[ 'get/' + path ] = func;
	}
	/**
	 * set
	 * @param { string } path 
	 * @param {callback} func 
	 */
	async set(path, func) {
		this._paths['set/' +path] = func;
	}
	/**
	 * del
	 * @param {string} path 
	 * @param {callback} func 
	 */
	async del(path, func) {
		this._paths['del/' +path] = func;
	}
	/**
	 * Fire Api 
	 * @param { object } params 
	 * @returns {object} this._paths
	 */
	async Fire({ path, socket, resolve= () => { }, datas = { }  }) {
		if( typeof  this._paths[path] === 'undefined') return false;
		if( typeof socket !== 'undefined' ) datas.socket = socket;
		return this._paths[ path ]( datas, resolve );
	}
	/**
	 * Ajax Fire 
	 * @param { object } params 
	 * @returns { promise } this._paths
	 */
	async AjaxFire({ path,  datas = { }  }) {
		if( typeof  this._paths[path] === 'undefined') return false;
		return new Promise(  async ( resolve ) => {
			this._paths[ path ]( datas, resolve );
		})
	}
	/**
	 * add Event listener to socket
	 * @param { object } socket 
	 * @param { object } io 
	 */
	async Events( socket, io ) {
		for( let path in this._paths ) {
			socket.on( path , (datas, callback = () => { } ) => {
				//console.log( path );
				this.Fire({ path , socket, datas, resolve: callback });
			} )

		}
	}

}
const Api = new ApiClass();
Store.Api = Api;



import ( '../api/index.mjs').then(() => {
	console.log('Loaded API');
})

export { Api } ;



