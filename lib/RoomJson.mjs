import fs from 'fs';
import { DATAS_PATH } from '../config/Paths.mjs'; 

const defo = {
    id: 0, vport: 0 , aport: 0 , etat: 0 , viewers: 0 
}

export const RoomJson = async (roomId, ndatas = false) => {
    var file = DATAS_PATH + 'room-' + roomId + '.json';
    const test = await fs.existsSync( file );
    var json = { ...defo, id:   roomId }; 
    if( test ) {
        var d = JSON.parse( await fs.readFileSync( file  ) );
        json = { ...json, ...d }; 
    }
    if ( ndatas !== false ) {
        json = { ...json, ...ndatas };
        await fs.writeFileSync( file, JSON.stringify( json ) )
    }
    return json; 
}

