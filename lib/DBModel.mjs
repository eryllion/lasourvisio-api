import { DB } from '../DB/index.mjs';
import {empty} from "./index.mjs";
import { DatasDecode } from './Tools.mjs';


class DBModel {
	/**
	 * 
	 * @param { number } id 
	 */
	constructor(id = 0 	) {
		this.id = id;
		this._datas = [];
		this._jsons = {};
		this._json_fields = [];
		this._id_name = 'id';
		this._tb = '';
	}
	/**
	 * Initialize 
	 * @param { number } id 
	 * @returns { boolean }
	 */
	async __Init(id) {
		if (this._tb == '') return this;
		const datas = await DB.Select( this._tb ).Where({ [this._id_name] : this.id }).Result();
		if (typeof datas[ this._id_name ] == 'undefined') {
			this.id = 0;
			return this;
		}
		this._datas = datas;
		if (this._json_fields.length > 0) {
			for( let i in this._json_fields) {
				const f = this._json_fields[i];
				if ( empty( this._datas[f]  )) {
					this._jsons[ f ] = {};
				} else {
					this._jsons[ f ] = JSON.parse( this._datas[f]);
				}
			}
		}
		return true;
	}
	/**
	 * Get By Key
	 * @param  {string} key 
	 * @param {string} defaut 
	 * @returns 
	 */
	Get(key, defaut = '' ) {
		if (typeof  this._datas[key] == 'undefined') return defaut;
		return this._datas[key];
	}
	/**
	 * Set value
	 * @param {string} key 
	 * @param {string} value 
	 * @returns {this}
	 */
	Set(key, value ) {
		this._datas[key] = value;
		return this;
	}
	/**
	 * Set Values
	 * @param {object} values 
	 * @returns {this}
	 */
	SetValues(values) {
		this._datas = { ...this._datas, ...values};
		return this;
	}
	/**
	 * Save to database
	 * @param {boolean} debug 
	 * @returns { boolean }
	 */
	async Save( debug = false ) {
		if (this._tb == '') return false;
		for( let i in this._jsons) {
			this._datas[ i ] = JSON.stringify( this._jsons[i] );
		}

		const values = this._datas;
		if (this.id > 0) {
			DB.Update(this._tb).Values(values).Where({ [this._id_name] : this.id }).Run(debug);
			return await this.__Init(this.id);
		} else {
			const R = await DB.Insert(this._tb).Values(values).Run(debug);
			this.id = R.insertId;
			return await this.__Init( R.insertId );
		}
	}
	/**
	 * Del from Database
	 * @returns {boolean}
	 */
	async Del() {
		if (this._tb == '' || this.id == 0 ) return false;
		await DB.Delete(this._tb).Where({ [this._id_name]: this.id }).Run();
		return true;
	}

	/**
	 * 
	 * @param key 
	 * @param json 
	 * @returns 
	 */
	async Decode( key , json = false ) {
		if( typeof this._datas[ key ] === 'undefined') return false;
		this._datas[ key ] = await DatasDecode( this._datas[ key ] , json ); 
	}
	/**
	 * 
	 * @param key 
	 * @param json 
	 * @returns 
	 */
	async Encode( key , json ) {
		if( typeof this._datas[ key ] === 'undefined') return false;
		this._datas[ key ] = await DatasEncode( this._datas[ key ] , json ); 
	}


}


export { DBModel };
