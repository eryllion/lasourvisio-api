import moment  from 'moment';
import crypto from 'crypto';
import CryptoJS from 'crypto-js';
import { SITE_KEY, SQL_DATE_FORMAT } from '../config/index.mjs';
import Base64 from 'nodejs-base64';
import { Store } from './index.mjs';

export const { base64encode, base64decode } = Base64;

/**
 * sleep
 * @param {number} ms 
 * @returns {promise} 
 */
export const sleep = ( ms ) => {
    return new Promise( res => setTimeout(res, ms) );
}

/**
 * Isset
 * @param variable 
 * @returns {boolean}
 */
export const Isset = (variable) => {
	if (typeof  variable == 'undefined') return false;
	return true;
}

/**
 * empty
 * @param value 
 * @returns {boolean}
 */
export const empty = (value) => {
	
	if ( typeof value == 'undefined') return true;
	if ( typeof value === 'string') {
		if ( value === '' ) return true;
		var t = Number( value );
		
		if ( isNaN( t ) || t === 'NaN') {
			return false; 
		}
	}
	if( typeof value === 'array' ) {
		if ( value.length === 0 ) return true; 
		return false; 
	}
	var t = Number( value );
	if ( !isNaN( t )) {
		if( parseInt( t , 10 ) === 0 ) return true ;
		return false;
	}
	if( value === null ) return true;
	return false; 
}

/**
 * InArray 
 * @param value 
 * @param {array} array 
 * @returns {boolean}
 */
export const InArray = (value, array) => {
	if (array.length == 0) return false;
	for( let i in array) {
		if (array[i] === value) return true;
	}
	return false;
}

/**
 * String Generator -  Generate a random string
 * @param {number} length   
 * @param {boolean} num 		- with numeric character
 * @param {boolean} special 	- with special character
 * @param {boolean} UpperCase   - with upper character
 * @param {boolean} Alpha 		- with alphabetics character
 * @returns { string }
 */
export const StringGenerator = (length = 10, num = true, special = true, UpperCase = true, Alpha = false ) => {
	let string = '';
	let chain = '';
	if (Alpha) chain += 'abcdefghijklmnopqrstuvwxyz';
	if( UpperCase) chain += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	if (num) chain +='1234567890';
	if (special) chain += "&#'(-@)][+=Â¤!:;";
	const cmax = chain.length;
	for( let i = 0; i < length; i++) {
		string += chain.charAt( Math.floor( Math.random() * cmax ) );
	}
	return string;
}

/**
 * IsJson - detect if the string is a correct JSON 
 * @param {string} str 
 * @returns {boolean}
 */
export const IsJson = (str) => {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}

/**
 * Alias JSON.stringify 
 * @param {object|array} data 
 * @returns {string}
 */
export const JSONSTR = (data)  => {
	return JSON.stringify(data).toString();
}

/**
 * Get Current DateTime
 * @returns {string}
 */
export const GetNow = () =>  {
	return moment().format(SQL_DATE_FORMAT );
}


export const Missing = async ( source, keys  ) => {
	for ( let i in keys ) {
		if ( empty( source[ keys[i] ] )) return true;
	}
	return false;
}



export const Crypt = async ( string ) => {
	var key = await CryptoJS.enc.Hex.parse('43297f767c3cf8de87f848a297a7dae8');
	var iv  = await CryptoJS.enc.Hex.parse('0237c67313c9a13956af082f7a440c09');

	var encrypted = await CryptoJS.AES.encrypt( string , key, { iv });

	var crypted_base64 =  await encrypted.ciphertext.toString(CryptoJS.enc.Base64);
	return crypted_base64;
}

export const DeCrypt = async ( crypted  ) => {
	var key = await CryptoJS.enc.Hex.parse('43297f767c3cf8de87f848a297a7dae8');
	var iv  = await CryptoJS.enc.Hex.parse('0237c67313c9a13956af082f7a440c09');
	let bytes = await CryptoJS.AES.decrypt(crypted, key, { iv });
	let decrypted = bytes.toString(CryptoJS.enc.Utf8);
	return decrypted;
}


export const EncodePass = ($pass ) => {
	let string = SITE_KEY + $pass + SITE_KEY + $pass;
	return crypto.createHash('md5').update(string).digest('hex');
}


/** deprecated */
export function number_format (number, decimals, dec_point, thousands_sep) {
	// Strip all characters but numerical ones.
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function (n, prec) {
			var k = Math.pow(10, prec);
			return '' + Math.round(n * k) / k;
		};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}




export const KeyValues = ( object ) => {
	for ( let i in object ) {
		return { key: i, value: object[i]};
	}
}


export function IsObject( data ) {
	if( typeof  data === 'object') return true;
	false;
}
export function IsString( data ) {
	if( typeof  data === 'string') return true;
	false;
}
export function IsArray( data ) {
	if( typeof  data === 'array') return true;
	false;
}
export function IsNumber( data ) {
	if( typeof  data === 'number') return true;
	false;
}

/**
 * 
 * @param {string} email 
 * @returns {boolean}
 */
export const CheckMail= ( email ) => {
	return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

/**
 * Check if the pass word is correct
 * @param {string} password 
 * @param {number} maxLength 
 * @param {boolean} num 
 * @param {boolean} upper 
 * @param {boolean} sp 
 * @returns 
 */
export const CheckPass = (password, maxLength = 6 , num = true, upper = true, sp = true  ) => {
	if( empty(password)) return false;
	if( password.length < maxLength ) return false;
	if (upper)
		if ( (password.match(/[A-Z]/g) || []).length  === 0 ) return false;
	if (num)
		if ( (password.match(/[0-9]/g) || []).length  === 0 ) return false;
	if ( (password.match(/[a-z]/g) || []).length  === 0 ) return false;
	if (sp )
		if ( (password.match(/[^a-zA-Z0-9]/g) || []).length  === 0 ) return false;
	return true;
}

/**
 * 
 * @param {string} str 
 * @returns {string}
 */
export const nl2br = ( str ) => {
	var tag = '<br />'; 
	return String(str).replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + tag + '$2');
}

export const Assign = async ( str, datas ) => {
	for( let i in datas ) {
		var r = new RegExp('\\{\\$' + i + '\\}' , 'g');
		str =  str.replace( r , datas[i] );
	}
	return str; 
}


export const strispslashes = (str ) => {
	if ( typeof str === 'undefined') {
		console.trace( str );
		return ''; 
	}
	var reg = new RegExp('\\\\', 'g');
	return str.replace( reg , '' ); 
}
export const stripslashes = strispslashes; 

export const NoNull = ( str, d = '' ) => {
	if( empty( str )) return d;
	if ( isNaN( str )) return d;
	if( str === null ) return d;
	if ( str.toString().toLower() === 'null') return d ;
	return str; 
}

export const NoNr = ( string ) => {
	return string.replace('\n' , '' ).replace('\r' , '' ).toString();
}
export const NrBr = ( string ) => {
	return string.replace('\n' , '<br />' ).replace('\r' , '<br />' ).toString();
}

export const IntRand = ( min, max ) => {
	return Math.floor(Math.random()*(max-min+1)+min);
}

export const SecondsToMinutes = ( s ) => {
	if ( s < 60 ) return 0;
      if ( s === 60 ) return 1 ;
      return parseInt( s / 60 ) 
}


export const DatasDecode = async ( source, json = false )  => {
	source = await base64decode( source ); 
	if ( json ) {
		return await JSON.parse( source );
	} 
	return source; 
}

export const DatasEncode = async ( source, json = false ) => {
	if( json ) source = await JSON.stringify( source );
	return await base64encode( source );
}