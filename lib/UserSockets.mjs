import { Store , Log} from './index.mjs';
import { Sockets } from "../classes/index.mjs";

const { Api }  = Store;


export class UserSockets {
    /**
     * 
     * @param socket 
     */
    constructor( socket ) {
        this._init( socket );
    }
    /**
     * init
     * @param socket 
     */
    _init( socket ) {
        this.socket =socket;
        this.socketid = socket.id;
        this.uid = 0;
        this.uuid = "";
        this.loggued = false;
        const { lang = 'en' , uuid=  '' } = this.socket.handshake.query;
		const siteDatas = { };
        this.uuid = uuid; 
        this.socket.uuid = uuid; 
		this.socket.emit('connected', { siteDatas  } );
        Log.add('NEW_SOCKET - connected ', { socketid: socket.id, uuid  }); 
        Sockets.LoginSocket( { socketid:  socket.id, uid:  0 , uuid,etat:  0  }); 
		this.InitEventAndListeners();
    }
    /**
     * Disconnect
     * @param socket 
     * @param callback 
     * @returns {boolean}
     */
    async Disconnect(socket, callback = () => { }) {
		const { id } = socket;
        Log.add('SOCKET_DISCONNECT', { socketid:id  }); 
		const Socket = await Sockets.InitBySocketId( id );
        const { _datas =  false } = Socket; 
        if( !_datas || typeof _datas === 'undefined' ) return false; 
		const { uid = 0 } =_datas;
		Socket._datas.etat = 0 ;
		Socket.Save();
        return true;
	}
  
	InitEventAndListeners() {
		this.socket.on('disconnect', () => {
			this.Disconnect( this.socket );
			console.log('Bye '  );
		} );
		Api.Events( this.socket , Store.io );
        Store.Rooms.Events( this.socket );
	}
}