import { DB } from '../DB/index.mjs';
import moment from 'moment';
import { GetNow } from '../lib/Tools.mjs'; 
import { LOGS_DB } from '../config/database.mjs'; 

export const Log = {
	/**
	 * Add Log
	 * @param  { string } key 
	 * @param data 
	 */
	add: async  (key, data ) => {
		Log.DelOld();
		console.log(key, data);
		const message = typeof data == 'object' ? JSON.stringify(data).toString() : data.toString() ;

		const values = {
			datecreate: await GetNow(),
			title: key,
			message: message ,
		};
		DB.Insert( LOGS_DB ).Values(values).Run();
	},
	/**
	 * Del Old logs
	 * @returns {boolean }
	 */
	DelOld: async ( ) => {
		const d = moment().subtract(2, 'days').format('YYYY-MM-DD HH:mm:ss');
		await DB.Delete( LOGS_DB ).Where({
			datecreate: {_lt: d}
		}).Run();
		return true; 
	},
}
