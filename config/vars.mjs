import fs from 'fs';

export const SITE_KEY = 'ssdf1ert5622gdf3r66t6z322df1';
export const mailjet_cfg = {
   MAILJET_API_KEY: '',
   MAILJET_API_SECRET: ""
}; 
export const timezone_cfg = {
    user: '',
    password: ''
}

export const linkedin_cfg =  {
    client_id: '',
   client_secret: ''
}

export const TZ = "Europe/Paris" ; 

export const cert_pem = '/etc/letsencrypt/live/lasour.sytes.net/fullchain.pem';
export const cert_key = '/etc/letsencrypt/live/lasour.sytes.net/privkey.pem';
export const httpsOptions = {
    key: fs.readFileSync( cert_key ),
    cert: fs.readFileSync( cert_pem ),
};

export const option_sockets = {
    pingInterval: ( 60 * 60000 ),
    pingTimeout: 5000,
    cookie: false,
};

export const TYPEFORM_SECRET = ""; 
export const TYPEFORM_API_TOKEN = ""; 

export const lang = "fr";

