export  const ROOTPATH = "/var/apps/lasourvisioapi/" ; 
export const DOMAINE  = 'lasour.sytes.net';

export const IMAGE_PATH = '/var/www/images/';
export const IMAGE_URI = 'https://images.' + DOMAINE + '/'; 

export const DATAS_PATH =  ROOTPATH + "datas/"; 

//CACHES
export const CACHEIMAGE = IMAGE_PATH;
export const CACHEJSON = "/var/caches/"; 
export const SCHEMAS = ROOTPATH +  "/config/schemas/"; 


