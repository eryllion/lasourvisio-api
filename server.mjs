import { Store } from './lib/Store.mjs'; 
import { Log } from './lib/Log.mjs'; 
import { TYPEFORM_SECRET  } from './config/index.mjs'; 
import bodyParser from 'body-parser'; 
import crypto from 'crypto';
import CryptoJS from 'crypto-js';
import express from 'express';


const { app } = Store; 

app.use(bodyParser.raw({ type: 'application/json' }));
var jsonParser = bodyParser.json()
//app.use( express.json({"Content-Type": "application/json"})); 

app.get('/',  async ( req, res ) => {
    res.send('Who are you ?');
})

var ACCES_URL = "https://lasour.sytes.net"; 

  app.post('/co/', async ( req, res ) => {
    
    res.setHeader('Access-Control-Allow-Origin', ACCES_URL );
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);

    res.header('Access-Control-Allow-Origin', ACCES_URL );
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Credentials', true);
   
    var content =   req.body.toString() ; 
    if ( content === '' ) {
      res.status(200).send({ etat: 0 });
      return false;
    }
    var json = JSON.parse( content ); 
    console.log( " ====== > ", json );  
    Log.add('CONNECT_FROM', { referer: req.headers.referer, ...json  });
    const siteDatas = { };
    res.status(200).send({ etat: 200 , siteDatas });
    return ''; 
  });


const Cors = ( res ) =>  {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Credentials', true);
  return res; 
}  

app.post( '/api/' , async (req, res ) => {
    await Cors( res ); 
    var referer  = req.headers.referer ;
    //if( referer.indexOf( ACCES_URL )  !== 0) return  res.status(200).send( { etat: 0 } ); 
   
    var content =   req.body.toString() ; 
    var json = JSON.parse( content ); 
    const { Api } = Store;
    const { path = '', datas = { }} = json;
    const r = await Api.AjaxFire({ path:  path,   datas });
    res.status(200).send( r ); 

    return '';
});

